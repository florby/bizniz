#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

rm -f $SCRIPTPATH/testsite/db.sqlite3
rm -rf $SCRIPTPATH/bizniz/migrations
python3 $SCRIPTPATH/testsite/manage.py makemigrations 
python3 $SCRIPTPATH/testsite/manage.py migrate 
python3 $SCRIPTPATH/testsite/manage.py makemigrations bizniz
python3 $SCRIPTPATH/testsite/manage.py migrate bizniz
