from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers

# from bizniz.views import TestView, BaseView, SignInView, SignOutView
# from bizniz.views import DashboardView, SearchView, OrdersView, InvoicesView, DeliveriesView, ProductionView, InventoryView, ArticlesView, ProductsView
# from bizniz.views.organisation import OrganisationsView, OrganisationView

from bizniz.rest.organisation import OrganisationViewSet, AddressViewSet, PersonViewSet, CustomerViewSet


app_name = "bizniz"

router = routers.DefaultRouter()
router.register("organisation", OrganisationViewSet, base_name="organisation")
router.register("address", AddressViewSet, base_name="address")
router.register("person", PersonViewSet, base_name="person")
router.register("customer", CustomerViewSet, base_name="customer")
#router.register("searchorganisation", SearchOrganisationViewSet, base_name="searchorganisation")

urlpatterns = [
    url("^", include(router.urls)),
    # url("^searchorganisation", SearchOrganisationView.as_view(), name="searchorganisation"),
    # path("", DashboardView.as_view(), name="dashboard"),
    # path("test/", TestView.as_view(), name="test"),
    # path("signin/", SignInView.as_view(), name="signin"),
    # path("signout/", SignOutView.as_view(), name="signout"),    
    # path("search/", SearchView.as_view(), name="search"),    
    # path("organisations/", OrganisationsView.as_view(), name="organisations"),    
    # path("organisation/", OrganisationView.as_view(), name="organisation"),
    # path("orders/", OrdersView.as_view(), name="orders"),    
    # path("invoices/", InvoicesView.as_view(), name="invoices"),    
    # path("deliveries/", DeliveriesView.as_view(), name="deliveries"),    
    # path("production/", ProductionView.as_view(), name="production"),    
    # path("inventory/", InventoryView.as_view(), name="inventory"),    
    # path("articles/", ArticlesView.as_view(), name="articles"),    
    # path("products/", ProductsView.as_view(), name="products"),    
]
