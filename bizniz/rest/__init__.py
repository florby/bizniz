from collections import OrderedDict
import decimal
import urllib

from django.db import models
from django.core import exceptions
from django.db.models import Q

from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import metadata
from rest_framework import pagination
from rest_framework import filters
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response


from bizniz import utils


def coerce_field_type_value(field_type, value):
    if not isinstance(field_type, type):
        field_type = type(field_type)
    if issubclass(field_type, (serializers.CharField, models.CharField)):
        return utils.to_string(value)
    elif issubclass(field_type, (serializers.IntegerField, models.IntegerField, models.AutoField)):
        return utils.to_integer(value)
    elif issubclass(field_type, (serializers.DecimalField, models.DecimalField)):
        return utils.to_decimal(value)
    elif issubclass(field_type, (serializers.DateField, models.DateField)):
        return utils.to_date(value)
    elif issubclass(field_type, (serializers.TimeField, models.TimeField)):
        return utils.to_time(value)
    elif issubclass(field_type, (serializers.DateTimeField, models.DateTimeField)):
        return utils.to_datetime(value)
    return None


def coerce_field_type_values(field_type, values):
    if not isinstance(field_type, type):
        field_type = type(field_type)
    return [coerce_field_type_value(field_type, v) for v in values]



def dig_model_field_from_param_name(param_name, model):
    spl = utils.escaped_split(param_name, ":", 1)
    field_names = utils.escaped_split(spl[0], ".")
    using_field = None
    for field_name in field_names:
        try:
            model_field = model._meta.get_field(field_name)
        except exceptions.FieldDoesNotExist:
            return None
        if hasattr(model_field, "related_model") and model_field.related_model:
            model = model_field.related_model
            continue
        using_field = model_field
    return using_field


class Pagination(pagination.PageNumberPagination):
    page_size = 25
    page_size_query_param = "page_size"
    max_page_size = 100


class LookupFilterError(ValidationError):
    status_code = 400
    default_code = "invalid_filter"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.param = kwargs.get("param")


class KeywordFilterError(ValidationError):
    status_code = 400
    default_code = "invalid_filter"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.param = kwargs.get("param")


class LookupFilter(filters.BaseFilterBackend):

    LOOKUP_ALIASES = {
        "c": "contains",
        "ic": "icontains",
        "e": "exact",
        "ie": "iexact",
        "r": "regex",
        "ir": "iregex",
        "sw": "startswith",
        "isw": "istartswith",
        "ew": "endswith",
        "iew": "iendswith",
        "ge": "gte",
        "le": "lte",
    }

    ALLOWED_LOOKUPS = [
        "contains",
        "icontains",
        "exact",
        "iexact",
        "regex",
        "iregex",
        "startswith",
        "istartswith",
        "endswith",
        "iendswith",
        "gt",
        "gte",
        "lt",
        "lte",
        "range",
        "year",
        "month",
        "day",
        "quarter",
        "hour",
        "minute",
        "second",
        "date",
        "time",
        "in",
    ]

    @classmethod
    def parse_param_name(cls, param):
        spl = utils.escaped_split(param, ":", 1)
        fields = utils.escaped_split(spl[0], ".")
        if len(spl) == 2:
            lookups = utils.escaped_split(spl[1], ":")
        else:
            lookups = []
        lookups = [cls.LOOKUP_ALIASES.get(l, l) for l in lookups]
        return (fields, lookups)


    def filter_queryset(self, request, queryset, view):
        if hasattr(view, "lookup_fields"):
            q = Q()
            for k, v in request.query_params.items():                
                param = "%s=%s"%(k, v)
                if k.startswith("c_"):
                    field_names, lookups = self.parse_param_name(k)
                    if not field_names:
                        raise LookupFilterError({"message": "Invalid lookup filter syntax", "param": param})
                    joined_field_name = "__".join(field_names)
                    if not joined_field_name in view.lookup_fields:
                        raise LookupFilterError({"message": "Invalid lookup filter field", "param": param})
                    # model = view.serializer_class.Meta.model
                    # using_field = None
                    # for field_name in field_names:
                    #     try:
                    #         model_field = model._meta.get_field(field_name)
                    #     except exceptions.FieldDoesNotExist:
                    #         using_field = None
                    #         break
                    #     if hasattr(model_field, "related_model") and model_field.related_model:
                    #         model = model_field.related_model
                    #         continue
                    #     using_field = model_field
                    using_field = dig_model_field_from_param_name(k, view.serializer_class.Meta.model)
                    if not using_field:
                        raise LookupFilterError({"message": "Invalid lookup filter field", "param": param})
                    output_field = using_field
                    value_splits = 0
                    for lookup in lookups:
                        if not lookup in self.ALLOWED_LOOKUPS:
                            raise LookupFilterError({"message": "Invalid lookup filter operation", "param": param})
                        lu = output_field.get_lookup(lookup)
                        if not lu:
                            lu = output_field.get_transform(lookup)
                        if not lu:
                            raise LookupFilterError({"message": "Invalid lookup filter operation", "param": param})
                        if issubclass(lu, models.lookups.Range):
                            value_splits = 1
                        elif issubclass(lu, models.lookups.In):
                            value_splits = -1
                        if hasattr(lu, "output_field"):
                            output_field = lu.output_field
                    raw_values = utils.escaped_split(v, "|", value_splits)
                    values = coerce_field_type_values(output_field, raw_values)
                    if None in values:
                        raise LookupFilterError({"message": "Invalid lookup filter value", "param": param})
                    if value_splits > 0 and value_splits + 1 != len(values):
                        raise LookupFilterError({"message": "Invalid lookup filter value array", "param": param})
                    key = "%s%s"%(joined_field_name, "__%s"%"__".join(lookups) if lookups else "")
                    if value_splits == 0:
                        values = values[0]
                    q &= Q(**{key: values})
            if q:
                try:
                    queryset = queryset.filter(q)
                except (exceptions.FieldError, ValueError, exceptions.FieldDoesNotExist, IndexError, KeyError):
                    raise LookupFilterError({"message": "Lookup filter execution failed"})
        return queryset


class Keyword:

    def __init__(self, name, serializer_field_type, func=None):
        self.name = name
        self.field_type = serializer_field_type
        self.func = func

    def get_query(self, view, value):
        if isinstance(self.func, str):
            attr = getattr(view, func, None)
        elif callable(self.func):
            attr = self.func
        elif isinstance(self.func, Q):
            return Q(self.func)
        else:
            attr = getattr(view, "keyword_query_%s"%self.name)
        if callable(attr):
            return attr(value)
        return Q()
    

class KeywordFilter(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        if hasattr(view, "_keywords"):
            q = Q()
            for k, v in request.query_params.items():
                param = "%s=%s"%(k, v)
                if k.startswith("k_"):
                    keyword = view._keywords.get(k)
                    if not keyword:
                        raise KeywordFilterError({"message": "Invalid keyword filter field", "param": param})
                    value = coerce_field_type_value(keyword.field_type, v)
                    if value is None:
                        raise KeywordFilterError({"message": "Invalid keyword filter value", "param": param})
                    qd = keyword.get_query(view, value)
                    if qd:
                        q &= qd
            if q:
                try:
                    queryset = queryset.filter(q)
                except (exceptions.FieldError, ValueError, exceptions.FieldDoesNotExist, IndexError, KeyError):
                    raise KeywordFilterError({"message": "Keyword filter execution failed", "param": param})
        return queryset
    

class BaseSerializer(serializers.ModelSerializer):
    """
    Serves as a base for all the ManagedModel-based serializers.
    """

    class Meta:
        fields = [
            "c_id",
            "c_created_datetime",
            "c_modified_datetime",
            "c_deleted_datetime",
            "c_created_by",
            "c_modified_by",
            "c_deleted_by",
        ]
        read_only_fields = list(fields)


class BaseViewSet(viewsets.ModelViewSet):
    pagination_class = Pagination
    filter_backends = [filters.OrderingFilter, LookupFilter, KeywordFilter]
    ordering_fields = list(BaseSerializer.Meta.fields)
    lookup_fields = list(BaseSerializer.Meta.fields)
    keywords = [
        Keyword("k_last_modified_start", serializers.DateTimeField),
        Keyword("k_last_modified_end", serializers.DateTimeField),
        Keyword("k_last_modified_by", serializers.CharField),
    ]

    
    def get_queryset(self):
        if not hasattr(self, "_keywords"):
            self._keywords = {}
            for keyword in self.keywords:
                self._keywords[keyword.name] = keyword
        for keyword in self.keywords:
            if not keyword.name in self._keywords:
                self._keywords[keyword.name] = keyword
        return self.serializer_class.Meta.model.objects.all()
    
    def list(self, request):
        ret = super().list(request)
        return ret
        
    def keyword_query_k_last_modified_start(self, value):
        return Q(c_created_datetime__gte=value) | Q(c_modified_datetime__gte=value) | Q(c_deleted_datetime__gte=value)
            
    def keyword_query_k_last_modified_end(self, value):
        return Q(c_created_datetime__lte=value) | Q(c_modified_datetime__lte=value) | Q(c_deleted_datetime__lte=value)

    def keyword_query_k_last_modified_by(self, value):
        return Q(c_created_by__icontains=value) | Q(c_modified_by__icontains=value) | Q(c_deleted_by__icontains=value)
