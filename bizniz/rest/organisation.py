from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import get_object_or_404

from rest_framework import serializers
from rest_framework import viewsets
from rest_framework.response import Response

from bizniz import utils
from bizniz.rest import BaseViewSet, BaseSerializer, Keyword
from bizniz.models.organisation import OrganisationModel, AddressModel, PersonModel, CustomerModel


class OrganisationSerializer(BaseSerializer):

    class Meta(BaseSerializer.Meta):
        model = OrganisationModel
        fields = list(BaseSerializer.Meta.fields) + [
            "c_name",
            "c_vat_number",
            "c_vat",
            "c_country",
            "c_currency",
            "c_notes",
            "c_website",
            "c_is_managed",
        ]



class AddressSerializer(BaseSerializer):

    class Meta(BaseSerializer.Meta):
        model = AddressModel
        fields = list(BaseSerializer.Meta.fields) + [
            "c_organisation",
            "c_name",
            "c_attention",
            "c_street",
            "c_area",
            "c_zipcode",
            "c_city",
            "c_country",
            "c_phone",
            "c_email",
            "c_instructions",
            "c_notes",
            "c_is_delivery",
            "c_is_invoice",
            "c_sort_index",
        ]
        read_only_fields = list(BaseSerializer.Meta.read_only_fields) + ["c_organisation"]
        



class PersonSerializer(serializers.ModelSerializer):

    class Meta(BaseSerializer.Meta):
        model = PersonModel
        fields = list(BaseSerializer.Meta.fields) + [
            "c_address",
            "c_name",
            "c_role",
            "c_gender",
            "c_phone",
            "c_email",
            "c_notes",
            "c_sort_index",
        ]
        read_only_fields = BaseSerializer.Meta.read_only_fields + ["c_address"]


class CustomerSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomerModel
        fields = list(BaseSerializer.Meta.fields) + [
            "c_organisation",
            "c_supplier",
            "c_customer_number",
        ]
        read_only_fields = BaseSerializer.Meta.read_only_fields + ["c_organisation", "c_supplier"]


class OrganisationViewSet(BaseViewSet):
    """
    Organisation.
    """
    serializer_class = OrganisationSerializer
    queryset = OrganisationModel.objects.all()
    ordering_fields = list(serializer_class.Meta.fields)
    lookup_fields = (
        list(serializer_class.Meta.fields)
        + ["c_addresses__%s"%f for f in AddressSerializer.Meta.fields]
        + ["c_addresses__c_persons__%s"%f for f in PersonSerializer.Meta.fields]
    )
    keywords = list(BaseViewSet.keywords) + [Keyword("k_text", serializers.CharField)]

    def keyword_query_k_text(self, value):
        return (Q(c_name__icontains=value)
                | Q(c_vat_number__icontains=value)
                | Q(c_country__icontains=value)
                | Q(c_website__icontains=value)
                | Q(c_addresses__c_name__icontains=value)
                | Q(c_addresses__c_attention__icontains=value)
                | Q(c_addresses__c_street__icontains=value)
                | Q(c_addresses__c_area__icontains=value)
                | Q(c_addresses__c_zipcode__icontains=value)
                | Q(c_addresses__c_city__icontains=value)
                | Q(c_addresses__c_country__icontains=value)
                | Q(c_addresses__c_persons__c_name__icontains=value)
                | Q(c_addresses__c_persons__c_phone__icontains=value)
                | Q(c_addresses__c_persons__c_email__icontains=value)
                | Q(c_addresses__c_persons__c_role__icontains=value)
        )
        

    
class AddressViewSet(BaseViewSet):
    """
    Address.
    """
    queryset = AddressModel.objects.all()
    serializer_class = AddressSerializer
    ordering_fields = list(serializer_class.Meta.fields)
    lookup_fields = (
        list(serializer_class.Meta.fields)
        + ["c_organisation__%s"%f for f in OrganisationSerializer.Meta.fields]
        + ["c_persons__%s"%f for f in PersonSerializer.Meta.fields]
    )


class PersonViewSet(BaseViewSet):
    """
    Person.
    """
    queryset = PersonModel.objects.all()
    serializer_class = PersonSerializer
    ordering_fields = list(serializer_class.Meta.fields)
    lookup_fields = (
        list(serializer_class.Meta.fields)
        + ["c_address__%s"%f for f in AddressSerializer.Meta.fields]
        + ["c_address__c_organisation__%s"%f for f in OrganisationSerializer.Meta.fields]
    )


class CustomerViewSet(BaseViewSet):
    """
    Customer.
    """
    queryset = CustomerModel.objects.all()
    serializer_class = CustomerSerializer
    ordering_fields = list(serializer_class.Meta.fields)
    lookup_fields = (
        list(serializer_class.Meta.fields)
        + ["c_organisation__%s"%f for f in OrganisationSerializer.Meta.fields]
        + ["c_supplier__%s"%f for f in OrganisationSerializer.Meta.fields]
        + ["c_organisation__c_addresses__%s"%f for f in AddressSerializer.Meta.fields]
        + ["c_supplier__c_addresses__%s"%f for f in AddressSerializer.Meta.fields]
        + ["c_organisation__c_addresses__c_persons__%s"%f for f in PersonSerializer.Meta.fields]
        + ["c_supplier__c_addresses__c_persons__%s"%f for f in PersonSerializer.Meta.fields]
    )


