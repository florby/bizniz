from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models import Q
from django.db.utils import IntegrityError

from bizniz.models import ManagedModel
from bizniz import utils


class OrganisationModel(ManagedModel):
    c_name = models.CharField(max_length=128, null=False, verbose_name="Name")
    c_vat_number = models.CharField(max_length=128, null=False, unique=True, verbose_name="VAT#")
    c_vat = models.DecimalField(max_digits=9, decimal_places=3)
    c_country = models.CharField(max_length=128, null=True, verbose_name="Country")
    c_currency = models.CharField(max_length=4, null=False, verbose_name="Currency")
    c_notes = models.TextField(null=True, verbose_name="Notes")
    c_website = models.CharField(max_length=256, verbose_name="website")
    c_is_managed = models.BooleanField(default=False, verbose_name="Is managed")
    

class AddressModel(ManagedModel):
    c_organisation = models.ForeignKey(OrganisationModel, on_delete=models.PROTECT, related_name="c_addresses", null=False, verbose_name="Organisation")
    c_name = models.CharField(max_length=128, null=False, verbose_name="Name")
    c_attention = models.CharField(max_length=256, null=True, verbose_name="Attention")
    c_street = models.CharField(max_length=256, null=True, verbose_name="Street")
    c_area = models.CharField(max_length=128, null=True, verbose_name="Area")
    c_zipcode = models.CharField(max_length=128, null=True, verbose_name="Zip code")
    c_city = models.CharField(max_length=128, null=True, verbose_name="City")
    c_country = models.CharField(max_length=128, null=True, verbose_name="Country")
    c_phone = models.CharField(max_length=256, verbose_name="Phone")
    c_email = models.CharField(max_length=256, verbose_name="E-mail")
    c_instructions = models.TextField(null=True, verbose_name="Instructions")
    c_notes = models.TextField(null=True, verbose_name="Notes")
    c_is_delivery = models.BooleanField(default=False, verbose_name="Is delivery")
    c_is_invoice = models.BooleanField(default=False, verbose_name="Is invoice")
    c_sort_index = models.IntegerField(null=False, default=0, verbose_name="Sort index")


class PersonModel(ManagedModel):
    c_address = models.ForeignKey(AddressModel, on_delete=models.PROTECT, related_name="c_persons", null=False, verbose_name="Address")
    c_name = models.CharField(max_length=128, null=False, verbose_name="Name")
    c_role = models.CharField(max_length=256, null=True, verbose_name="Role")
    c_gender = models.IntegerField(default=0, verbose_name="Gender")
    c_phone = models.CharField(max_length=256, verbose_name="Phone")
    c_email = models.CharField(max_length=256, verbose_name="E-mail")
    c_notes = models.TextField(null=True, verbose_name="Notes")
    c_sort_index = models.IntegerField(null=False, default=0)
    

class CustomerModel(ManagedModel):

    class Meta(ManagedModel.Meta):
        unique_together = (("c_organisation", "c_supplier"), ("c_supplier", "c_customer_number"))
    
    c_organisation = models.ForeignKey(OrganisationModel, on_delete=models.CASCADE, null=False, related_name="c_suppliers")
    c_supplier = models.ForeignKey(OrganisationModel, on_delete=models.CASCADE, null=False, related_name="c_customers")
    c_customer_number = models.CharField(max_length=32, null=False)
