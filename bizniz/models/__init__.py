from collections import OrderedDict
import datetime, decimal

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models import Q
from django.db.utils import IntegrityError

from bizniz import utils


UNIT_MAX_DIGITS = 32
UNIT_DECIMAL_PLACES = 15
        

class ManagedModel(models.Model):

    class Meta:
        abstract = True
        default_permissions = ()

    c_id = models.AutoField(primary_key=True, verbose_name="ID")
    c_created_datetime = models.DateTimeField(default=timezone.now, null=False, verbose_name="Created datetime")
    c_modified_datetime = models.DateTimeField(null=True, verbose_name="Modified datetime")
    c_deleted_datetime = models.DateTimeField(null=True, verbose_name="Deleted datetime")
    c_created_by = models.CharField(max_length=128, null=True, verbose_name="Created by")
    c_modified_by = models.CharField(max_length=128, null=True, verbose_name="Modified by")
    c_deleted_by = models.CharField(max_length=128, null=True, verbose_name="Deleted by")


