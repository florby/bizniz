# -*- coding: utf-8 -*-
"""
A small module with some utilities.
"""
import decimal
import datetime
from django.utils.timezone import is_aware, make_aware


DATE_FORMATS = ["%Y-%m-%d", "%Y%m%d", "%d/%m/%Y"]
TIME_FORMATS = ["%H:%M:%S", "%H%M%S", "%H:%M", "%H%M", "%H"]
TIME_FORMATS += ["%s%%z"%f for f in TIME_FORMATS] + ["%s %%z"%f for f in TIME_FORMATS] + ["%s%%Z"%f for f in TIME_FORMATS] + ["%s %%Z"%f for f in TIME_FORMATS]

DATETIME_FORMATS = list(DATE_FORMATS)
for df in DATE_FORMATS:
    DATETIME_FORMATS += ["%s%s"%(df, tf) for tf in TIME_FORMATS]
    DATETIME_FORMATS += ["%s %s"%(df, tf) for tf in TIME_FORMATS]


def to_integer(s, default=None):
    """
    Convert a string to an integer.
    """
    try:
        return int(s)
    except (ValueError, TypeError):
        pass
    return default


def is_integer(s):
    return not to_integer(s) is None


def to_float(s, default=None):
    """
    Convert a string to an integer.
    """
    try:
        return float(s)
    except (ValueError, TypeError):
        pass
    return default


def is_float(s):
    return not to_float(s) is None


def to_decimal(s, default=None):
    """
    Convert a string (or int, float, another decimal) to a decimal.
    None and empty strings return a decimal with value 0.
    """
    if s is None:
        return decimal.Decimal(0)
    if isinstance(s, str):
        s = s.strip()
        if not s:
            return decimal.Decimal(0)
    try:
        return decimal.Decimal(s)
    except (ValueError, TypeError, decimal.InvalidOperation):
        pass
    return default


def is_decimal(s):
    return not to_decimal(s) is None


def to_date(s, default=None):
    """
    Convert a string to a datetime.date.    
    """
    if isinstance(s, (datetime.date, datetime.datetime)):
        return datetime.date(s.year, s.month, s.day)
    if isinstance(s, str):
        s = s.strip()
        if s:
            for f in DATE_FORMATS:
                try:
                    return datetime.datetime.strptime(s, f).date()
                except ValueError:
                    continue
    return default


def is_date(s):
    return not to_date(s) is None


def to_time(s, default=None):
    """
    Convert a string to a datetime.time.    
    """
    if isinstance(s, (datetime.time, datetime.datetime)):
        r = datetime.time(s.hour, s.minute, s.second, s.microsecond, s.tzinfo)
        if not is_aware(r):
            r = make_aware(r)
    if isinstance(s, str):
        s = s.strip()
        if s:
            for f in TIME_FORMATS:
                try:
                    r = make_aware(datetime.datetime.strptime(s, f).time())
                except ValueError:
                    continue
                if not is_aware(r):
                    r = make_aware(r)
                    return r
    return default


def is_time(s):
    return not to_time(s) is None


def to_datetime(s, default=None):
    """
    Convert a string to a datetime.datetime.    
    """
    if isinstance(s, datetime.datetime):
        r = datetime.datetime(s.year, s.day, s.month, s.hour, s.minute, s.second, s.microsecond, s.tzinfo)
        if not is_aware(r):
            r = make_aware(r)
        return r
    if isinstance(s, str):
        s = s.strip()
        if s:
            for f in DATETIME_FORMATS:
                try:
                    r = datetime.datetime.strptime(s, f)
                except ValueError as e:
                    continue
                if not is_aware(r):
                    r = make_aware(r)
                return r
    return default


def is_datetime(s):
    return not to_datetime(s) is None


def to_bool(s, default=None):
    """
    Convert a string into a bool.
    """
    if isinstance(s, (int, float, bool, decimal.Decimal)):
        return bool(s)
    if isinstance(s, str):
        s = s.strip().lower()
        i = to_integer(s)
        if not i is None:
            return bool(i)
        if s in ("true", "on"):
            return True
        elif s == ("false", "off"):
            return False
    return default


def is_bool(s):
    return not to_bool(s) is None

def to_string(s, default=None):
    """
    Convert something into a string.
    Basically ensures that None is returned as an empty string instead of "None".
    """
    if s is None:
        return default
    return str(s)


def is_string(s):
    return not to_string(s) is None


def flatten_list(l, depth=-1):
    ret = []
    for i in l:
        if isinstance(i, (list, tuple)):
            if depth < 0:
                ret += flatten_list(i)
            elif depth > 0:
                ret += flatten_list(i, depth - 1)
            else:
                ret.append(i)
        else:
            ret.append(i)
    return ret


def type_name(instance_or_type):
    """
    Get the type name regardless of it being a type or not.
    """
    if hasattr(instance_or_type, "__name__"):
        return instance_or_type.__name__
    return type(instance_or_type).__name__


def posted_value(v):
    if isinstance(v, (str, int, bool, decimal.Decimal)):
        return v
    elif hasattr(v, "__getitem__") and hasattr(v, "__len__") and len(v) > 0:
        return v[0]
    return None
        

def format_decimal(d, thousands_sep="", decimal_point=".", max_decimals=-1):
    dec = to_decimal(d, decimal.Decimal(0))
    t = dec.as_tuple()
    if t.exponent >= 0:
        return "".join(("%d"%d for d in t.digits))
    decpoint = len(t.digits) + t.exponent
    pre_digits = t.digits[:decpoint]
    post_digits = []
    for i in range(1, decpoint):
        if t.digits[-i] != 0:
            post_digits = t.digits[decpoint:-i]
            break
    if post_digits:
        return "%s.%s"%("".join(("%d"%d for d in pre_digits)), "".join(("%d"%d for d in post_digits)))
    return "".join(("%d"%d for d in pre_digits))


def escaped_split(s, sep, max_splits=-1, escape="\\"):
    index = s.find(sep)
    if index < 0 or max_splits == 0:
        return [s]
    ret = []
    last_found = 0
    while index >= 0:
        escaped = False
        i = index - len(escape)
        while i >= 0 and s[i:i+len(escape)] == escape:
            escaped = not escaped
            i -= len(escape)
        if escaped:
            index = s.find(sep, index + len(sep))
        else:
            ts = s[last_found:index]
            spl = ts.split("%s%s"%(escape, escape))
            joiner = []
            for sp in spl:
                joiner.append(sp.replace("%s%s"%(escape, sep), sep))
            ret.append(escape.join(joiner))
            last_found = index + len(sep)
            index = s.find(sep, last_found)
        if max_splits > 0 and len(ret) >= max_splits:
            break
    if last_found <= len(s):
        ret.append(s[last_found:])
    return ret
    

if __name__ == "__main__":
    pass
