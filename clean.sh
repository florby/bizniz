#!/bin/bash

find . -iname "*~" | xargs rm -f
find . -iname "*.pyc" | xargs rm -f
find . -iname "__pycache__" | xargs rm -rf
